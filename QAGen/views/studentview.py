from django.views import View
from QAGen.forms.auth import *
from django.shortcuts import render,redirect
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.contrib.auth import authenticate,logout,login
from django.contrib import messages
from QAGen.models import *

import random
def getlist(count):
    s=set()
    while(len(s)!=10):
        s.add(random.randint(0,count))
    return list(s)
def fun(qalist):
    l=[(1, 2, 3, 4), (1, 2, 4, 3), (1, 3, 2, 4), (1, 3, 4, 2), (1, 4, 2, 3), (1, 4, 3, 2), (2, 1, 3, 4), (2, 1, 4, 3), (2, 3, 1, 4), (2, 3, 4, 1), (2, 4, 1, 3), (2, 4, 3, 1), (3, 1, 2, 4), (3, 1, 4, 2), (3, 2, 1, 4), (3, 2, 4, 1), (3, 4, 1, 2), (3, 4, 2, 1), (4, 1, 2, 3), (4, 1, 3, 2), (4, 2, 1, 3), (4, 2, 3, 1), (4, 3, 1, 2), (4, 3, 2, 1)]
    index=random.randint(0,len(l)-1)
    #print(index)
    li=[qalist.answer,qalist.option1,qalist.option2,qalist.option3]
    final=[li[i-1] for i in l[index]]
    return final
def myformat(qalist):
    final=[]
    count=1
    for each in qalist:
        d={}
        d['number']=count
        d['question']=each.question
        d['answer']=each.answer
        d['options']=fun(each)
        final.append(d)
        count+=1
    return final

def finallist(qalist):
    numbers = getlist(len(qalist)-1)
    finalanswers = [qalist[i] for i in numbers]
    return finalanswers

def getstatus(response):
    total=0
    count=0
    attempted=0
    k=response.keys()
    l=[]
    for each in k:
        values=each.split('/')
        val=response[each]
        if len(values)==2 and values[0]=='Question':
            d={}
            total+=1
            d['number']=total
            d['question']=values[1]
            d['answer']=val
            d['attempted']=False
            d['correct']=False
            if values[1]+"/"+val in k:
                d['attempted']=True
                attempted+=1
                res=response[values[1]+"/"+val]
                if res==val:
                    d['correct']=True
                    count+=1
            l.append(d)
    return (total,attempted,count,l)




class Taketest(View):


    def get(self,request,*args,**kwargs):
        taketest=takeTestForm()
        image = myuser.objects.get(user__username=request.user.username)
        return render(request,template_name='take_test.html',context={'taketest':taketest,'image':image})


    def post(self,request,*args,**kwargs):

        print('iosvdbhfjksf')
        taketest=takeTestForm(request.POST)
        qapairs=TestQA.objects.filter(name__name=request.POST['subject'],module__module=int(request.POST['unit']))
        finalanswers=finallist(qapairs)
        finalanswers=myformat(finalanswers)
        image = myuser.objects.get(user__username=request.user.username)
        return render(request,template_name='attempt_test.html',context={'qa':finalanswers,'subject':request.POST['subject'],'unit':request.POST['unit'],'image':image})

class EvaluateTest(View):
    def post(self,request,*args,**kwargs):
        result=getstatus(request.POST)
        image=myuser.objects.get(user__username=request.user.username)
        print(image.image)
        return render(request,
                      template_name="scorepage.html",
                      context={
                          'tot':result[0],
                          'att':result[1],
                          'correct':result[2],
                          'response':result[3],
                          'image':image,
                      }
        )


def Logout(request):
    logout(request)
    return redirect("home")


class LoginView(View):
    def get(self,request,*args,**kwargs):

        if request.user.is_authenticated:
            isteacher = User.objects.values('myuser__isteacher').get(username=request.user)
            if not isteacher['myuser__isteacher']:
                return redirect("studenthome")
        login=LoginForm()
        return render(request,template_name="login_page.html",context={'login':login,'uri':'student_signup_html'})
    def post(self,request,*args,**kwargs):
        Login=LoginForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        user=authenticate(request,username=username,password=password)
        isteacher = User.objects.values('myuser__isteacher').get(username=username)
        print(isteacher)
        if user is not None and not isteacher['myuser__isteacher']:
            login(request,user)
            return redirect("studenthome")
        else:
            messages.error(request,'invalid credentials')
        return render(request,template_name="login_page.html",context={'login':Login,'uri':'student_signup_html'})

class SignUpView(View):
    def get(self,request,*args,**kwargs):
        signup=SignUpForm()
        return render(request,template_name='signup_page.html',context={'signup':signup})
    def post(self,request,*args,**kwargs):
        signup=SignUpForm(request.POST,request.FILES)
        username=request.POST['username']
        try:
            if User.objects.get(username=username):
                messages.error(request, 'user already exists')
                return redirect("student_signup_html")
        except:
            if signup.is_valid():



                image=signup.cleaned_data.pop('image')
                isteacher = signup.cleaned_data.pop('isteacher')


                user=User.objects.create_user(**signup.cleaned_data)
                user.save()
                ipuser=myuser()
                if image is not None:
                    ipuser.image=image
                ipuser.isteacher=isteacher
                ipuser.user=user
                ipuser.save()

                if ipuser is not None:
                    login(request,user)
                    return redirect("studenthome")
                messages.error(request, 'signup failed')
                return redirect('student_signup_html')

class Home(View,LoginRequiredMixin):
    login_url= '/studentlogin/'
    def get(self,request):
        image = myuser.objects.get(user__username=request.user.username)
        return render(request,template_name="studenthome.html",context={'image':image})