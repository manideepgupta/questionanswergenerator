from django.db import models

# Create your models here.
from django.contrib.auth.models import User

class Subject(models.Model):
    name=models.CharField(max_length=64,null=True)

class Unit(models.Model):
    module=models.IntegerField()


class TestQA(models.Model):
    question=models.TextField(max_length=256,null=True)
    answer=models.TextField(max_length=64,null=True)
    option1=models.TextField(max_length=64,null=True)
    option2 = models.TextField(max_length=64, null=True)
    option3 = models.TextField(max_length=64, null=True)
    name=models.ForeignKey(Subject,on_delete=models.CASCADE)
    module=models.ForeignKey(Unit,on_delete=models.CASCADE)

class myuser(models.Model):
    image=models.ImageField(upload_to="gallery/",default="gallery/default.png")
    isteacher=models.BooleanField()
    user=models.ForeignKey(User,on_delete=models.CASCADE)
