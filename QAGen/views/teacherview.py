from django.views import View
from QAGen.forms.auth import *
from django.shortcuts import render,redirect
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.contrib.auth import authenticate,logout,login
from django.contrib import messages

from qagenerator.mcqgen import *
from QAGen.models import *

class createTestView(View):

    login_url = '/teacherlogin/'
    def get(self,request,*args,**kwargs):
        testform=createTestForm()
        image=myuser.objects.get(user__username=request.user.username)
        return render(
            request,
            template_name="create_test.html",
            context={'label': testform,'image':image}
        )
    def post(self,request,*args,**kwargs):
        se = createTestForm(request.POST, request.FILES)
        f=(request.FILES['file'].read())
        l=str(f)[2:-1]
        qa= generateQuestions(l,40)
        for i in qa:
            if len(i['distractors'])==3:
                qapair=TestQA()
                qapair.question = i['question']
                qapair.answer=i['answer']
                qapair.option1=i['distractors'][0]
                qapair.option2 = i['distractors'][1]
                qapair.option3 = i['distractors'][2]
                qapair.name=Subject.objects.get(name=request.POST['subject'])
                qapair.module=Unit.objects.get(module=int(request.POST['unit']))
                qapair.save()
        image = myuser.objects.get(user__username=request.user.username)
        return render(request,template_name='success_page.html',context={'subject':request.POST['subject'],'unit':request.POST['unit'],'image':image})




def Logout(request):
    logout(request)
    return redirect("home")


class LoginView(View):
    def get(self,request,*args,**kwargs):
        #isteacher = User.objects.values('myuser__isteacher').get(username=request.user)
        if request.user.is_authenticated :
            isteacher = User.objects.values('myuser__isteacher').get(username=request.user)
            if isteacher['myuser__isteacher']:
                return redirect("teacherhome")
        login=LoginForm()
        return render(request,template_name="login_page.html",context={'login':login,'uri':'teacher_signup_html'})
    def post(self,request,*args,**kwargs):
        Login=LoginForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        user=authenticate(request,username=username,password=password)
        isteacher = User.objects.values('myuser__isteacher').get(username=username)
        if user is not None and isteacher['myuser__isteacher']:
            login(request,user)
            return redirect("teacherhome")
        else:
            messages.error(request,'invalid credentials')
        return render(request,template_name="login_page.html",context={'login':Login,'uri':'teacher_signup_html'})

class SignUpView(View):
    def get(self,request,*args,**kwargs):
        signup=SignUpForm()
        return render(request,template_name='signup_page.html',context={'signup':signup})
    def post(self,request,*args,**kwargs):
        signup=SignUpForm(request.POST,request.FILES)
        username=request.POST['username']
        try:
            if User.objects.get(username=username):
                messages.error(request, 'user already exists')
                return redirect("teacher_signup_html")
        except:
            if signup.is_valid():



                image=signup.cleaned_data.pop('image')
                isteacher = signup.cleaned_data.pop('isteacher')

                print(isteacher)
                user=User.objects.create_user(**signup.cleaned_data)
                user.save()
                ipuser=myuser()
                if image is not None:
                    ipuser.image=image
                ipuser.isteacher=isteacher
                ipuser.user=user
                ipuser.save()

                if ipuser is not None:
                    login(request,user)
                    return redirect("teacherhome")
                messages.error(request, 'signup failed')
                return redirect('teacher_signup_html')

class Home(View,LoginRequiredMixin):
    login_url= '/teacherlogin/'
    def get(self,request):
        image = myuser.objects.get(user__username=request.user.username)
        return render(request,template_name="teacherhome.html",context={'image':image})