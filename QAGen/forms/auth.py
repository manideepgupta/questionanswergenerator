from django import forms
class createTestForm(forms.Form):


    subject_choices=((str(chr(65+i)),chr(65+i) ) for i in range(26))
    unit_choices=((str(i),str(i)) for i in range(1,6))
    subject=forms.ChoiceField(
        choices=subject_choices

    )
    unit=forms.ChoiceField(
       choices=unit_choices
    )
    file=forms.FileField()

class takeTestForm(forms.Form):
    subject_choices = ((str(chr(65 + i)), chr(65 + i)) for i in range(26))
    unit_choices = ((str(i), str(i)) for i in range(1, 6))
    subject = forms.ChoiceField(
        choices=subject_choices

    )
    unit = forms.ChoiceField(
        choices=unit_choices
    )


class LoginForm(forms.Form):
    username = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class':'input','placeholder':'username'})
    )
    password=forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={'class':'input','placeholder':'password'})
    )
class SignUpForm(forms.Form):
    first_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'firstname'})
    )
    last_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'lastname'})
    )
    username = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'regdno'})
    )
    password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'password'})
    )
    isteacher=forms.BooleanField(required=False)
    image = forms.ImageField(required=False)
