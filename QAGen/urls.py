from django.urls import path
from majorproject import settings
from QAGen.views import teacherview,studentview,userview
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns=[
    path('createtest/',teacherview.createTestView.as_view(),name="create_test_html"),
    path('taketest/',studentview.Taketest.as_view(),name="take_test_html"),
    path('evaluate/',studentview.EvaluateTest.as_view(),name="getscore"),
    path('home/',userview.Home.as_view(),name="home"),
    path('studentlogin/',studentview.LoginView.as_view(),name="student_login_html"),
    path('studentsignup/',studentview.SignUpView.as_view(),name="student_signup_html"),
    path('studentlogout/', studentview.Logout, name='logout_html'),
    path('teacherlogin/',teacherview.LoginView.as_view(),name="teacher_login_html"),
    path('teachersignup/',teacherview.SignUpView.as_view(),name="teacher_signup_html"),
    path('teacherlogout/', teacherview.Logout, name='teacher_logout_html'),
    path('studenthome/',studentview.Home.as_view(),name="studenthome"),
    path('teacherhome/', teacherview.Home.as_view(), name="teacherhome"),

]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)